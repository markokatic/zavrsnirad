class DvaStringa
{
    n1;
    n2;
};

var canvas = document.getElementById("canvas");

var graph = new Graph(canvas,1000,500);

var Fraction = algebra.Fraction;

var Expression = algebra.Expression;

var Equation = algebra.Equation;

function ucitajText(){
    let input = document.getElementById("input").value;
    return input;
}

function pronadiExpression(text)
{
    let ret = new DvaStringa();
    let n1 = "";
    let n2 = "";
    equalsSignFound = false;
    for(let i = 0;i<text.length;i++)
    {
        if(text[i]=='='){equalsSignFound=true;}
        if(text[i]!='='){
            if(equalsSignFound)
            {
                n2 = n2.concat(text[i]);
            }
            else
            {
                n1 = n1.concat(text[i]);
            }
        }
    }
    ret.n1=n1;
    ret.n2=n2;
    return ret;
}

function getResultString(abc)
{   
    var resultString = ""
    var a = abc[0]
    var b = abc[1]
    var c = abc[2]
    if(a>0)
    {
        resultString = resultString + a + "x<sup>2</sup>"
        if(b>0)
        {
            resultString = resultString + "+"+b+"x"
            if(c>0)
            {
                return resultString = resultString + "+"+c
            }
            else
            {
                if(c!=0)
                {
                    return resultString = resultString +c
                }
                else
                {
                    return resultString
                }
            }
        }
        else
        {
            if(b!=0)
            {
                resultString = resultString +b*(-1)+"x"
                if(c>0)
                {
                    return resultString = resultString + "+"+c
                }
                else
                {
                    if(c!=0)
                    {
                        return resultString = resultString +c
                    }
                    else
                    {
                        return resultString
                    }
                }
            }
        }

    }
    else
    {
        if(a!=0)
        {
            resultString =  a + "x<sup>2</sup>"
            if(b>0)
            {
                resultString = resultString + "+"+b+"x"
                if(c>0)
                {
                    return resultString = resultString + "+"+c
                }
                else
                {
                    if(c!=0)
                    {
                        return resultString = resultString +c
                    }
                    else
                    {
                        return resultString
                    }
                }
            }
            else
            {
                if(b!=0)
                {
                    resultString = resultString +b*(-1)+"x"
                    if(c>0)
                    {
                        return resultString = resultString + "+"+c
                    }
                    else
                    {
                        if(c!=0)
                        {
                            return resultString = resultString +c
                        }
                        else
                        {
                            return resultString
                        }
                    }
                }
            }
        }
    }
}

function getResults(abc)
{
    var a = abc[0]
    var b = abc[1]
    var c = abc[2]
    var x1 = 0
    var x2 = 0

    var D = b*b - 4*a*c
    if(D>=0)
    {
        x1 = (-b-Math.sqrt(D))/(2*a)
        x2 = (-b+Math.sqrt(D))/(2*a)
        return [x1,x2]
    }
    else
    {
        x1 = (-b-Math.sqrt(-D))/(2*a)
        x2 = (-b+Math.sqrt(-D))/(2*a)
        return [x1+"i",x2+"i"]
    }
}

function solve()
{
    graph.clearCanvas()

    text = ucitajText();
    let st = pronadiExpression(text);
    var n1 = algebra.parse(st.n1);
    var n2 = algebra.parse(st.n2);
    var quad = new Equation(n1, n2);
    var answers = quad.solveFor("x");
    if(answers.length==null)
    {
        document.getElementById("rjrj").innerHTML="x= "+answers.toString();
        graph.drawLinear(answers)
    }
    else
    {
        var abc = quad.getABC("x")
        var resultString = ""
        graph.drawParabola(abc[0],abc[1],abc[2])
        resultString = getResultString(abc)
        results = getResults(abc)
        document.getElementById("rjrj").innerHTML=resultString + " = 0"
        document.getElementById("rjrj").innerHTML += "<br>x1= "+results[0]+"<br>x2= "+results[1];

    }

}
function parseQuadratic()
{
    graph.clearCanvas()
    text = ucitajText();
    text2 = document.getElementById("input2").value
    var quad = new Sustav(text,text2)
    quad.registerCoefficients()
    quad.getLinear()
    var results = graph.drawObjects(quad.x2,quad.y2,quad.x,quad.y,quad.c,quad.a,quad.b)
    if(typeof(results) != "undefined")
    {
        if(results.length==2&&results[0]!=NaN)
        {
            document.getElementById("rjrj").innerHTML="Dotiču se u točki ("+results[0]+","+results[1]+")."
        }
        if(results.length==4&&results[0]!=NaN)
        {
            document.getElementById("rjrj").innerHTML="Presjecaju se u točkama ("+results[0]+","+results[1]+") i ("+results[2]+","+results[3]+")."
        }
        if(results=="err")
        {
            document.getElementById("rjrj").innerHTML="Nije moguće odrediti broj točaka"
        }

    }
    else
    {
        document.getElementById("rjrj").innerHTML="Nema zajedničkih točaka."
    }
}
