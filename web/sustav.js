class Sustav
{
    x=0;
    y=0;
    x2=0;
    y2=0;
    xy=0;
    x2y=0
    xy2=0
    x2y2=0
    c=0;
    a=0;
    b=0;
    nums=['0','1','2','3','4','5','6','7','8','9','.','-'];
    constructor(jednadzbaString,linearna)
    {
        this.jednadzbaString=jednadzbaString
        this.linearna = linearna
    }

    getCoefficients(rightSide,input)
    {
        var jednadzbaString = this.adaptString(input)
        var i = 0
        var j = 0
        var coefString = ""
        while(i<jednadzbaString.length)
        {
           if(this.nums.includes(jednadzbaString[i]))
           {
                j = i
                coefString=""
                while(this.nums.includes(jednadzbaString[j]))
                {

                    coefString+=jednadzbaString[j]

                    j++
                }
                i = j
                if(jednadzbaString[i]=='x')
                {
                    if(jednadzbaString[i+1]=='y')
                    {
                        if(jednadzbaString[i+2]=='^')
                        {
                            if(rightSide)
                            {
                                this.xy2 = -parseFloat(coefString)
                            }
                            else
                            {
                                this.xy2 = parseFloat(coefString)
                            }
                            i+=3
                        }   
                        else
                        {
                            if(rightSide)
                            {
                                this.xy = parseFloat(coefString)
                            }
                            else
                            {
                                this.xy = -parseFloat(coefString)
                            }
                            
                            i+=2
                        }
                    }
                    else if(jednadzbaString[i+1]=='^')
                    {
                        if(jednadzbaString[i+3]=='y')
                        {
                            if(jednadzbaString[i+4]=='^')
                            {
                                if(rightSide)
                                {
                                    this.x2y2 = -parseFloat(coefString)
                                }
                                else
                                {
                                    this.x2y2 = parseFloat(coefString)
                                }
                                
                                i += 5
                            }
                            else
                            {
                                if(rightSide)
                                {
                                    this.x2y = -parseFloat(coefString)
                                }
                                else
                                {
                                    this.x2y = parseFloat(coefString)
                                }
                                i += 3
                            }
                        }
                        else
                        {
                            if(rightSide)
                            {
                                this.x2 = -parseFloat(coefString)
                            }
                            else
                            {
                                this.x2 = parseFloat(coefString)
                            }
                            
                            i += 2
                        }
                    }
                    else
                    {
                        if(rightSide)
                        {
                            this.x = -parseFloat(coefString)
                        }
                        else
                        {
                            this.x = parseFloat(coefString)
                        }
                        
                        i++
                    }
                }
                else if(jednadzbaString[i]=='y')
                {
                    if(jednadzbaString[i+1]=='^')
                    {
                        if(jednadzbaString[i+3]=='x')
                        {
                            if(jednadzbaString[i+4]=='^')
                            {
                                if(rightSide)
                                {
                                    this.x2y2 = -parseFloat(coefString)
                                }
                                else
                                {
                                    this.x2y2 = parseFloat(coefString)
                                }
                                i+=5
                            }
                            else
                            {
                                if(rightSide)
                                {
                                    this.xy2 = -parseFloat(coefString)
                                }
                                else
                                {
                                    this.xy2 = parseFloat(coefString)
                                }
                                i+=4
                            }
                        }
                        else
                        {
                            if(rightSide)
                            {
                                this.y2 = -parseFloat(coefString)
                            }
                            else
                            {
                                this.y2 = parseFloat(coefString)
                            }
                            i += 2
                        }
                    }
                    else if(jednadzbaString[i+1]=='x')
                    {
                        if(jednadzbaString[x+2]=='^')
                        {
                            if(rightSide)
                            {
                                this.x2y = -parseFloat(coefString)
                            }
                            else
                            {
                                this.x2y = parseFloat(coefString)
                            }
                           
                            i+=3
                        }
                        else
                        {
                            
                            if(rightSide)
                            {
                                this.xy = -parseFloat(coefString)
                            }
                            else
                            {
                                this.xy = parseFloat(coefString)
                            }
                            i+=2
                        }
                    }
                    else
                    {
                        if(rightSide)
                        {
                            this.y = -parseFloat(coefString)
                        }
                        else
                        {
                            this.y = parseFloat(coefString)
                        }
                        i++
                    }
                }
                else
                {
                    if(rightSide)
                    {
                        this.c = -parseFloat(coefString)
                    }
                    else
                    {
                        this.c = parseFloat(coefString)
                    }
                }
           }
           i++
        }
    }

    adaptString(input)
    {
        var jednadzbaString =  input
        var i = 0
        jednadzbaString = jednadzbaString.split(' ').join('')
        jednadzbaString = jednadzbaString.split('+').join(' + ')
        jednadzbaString = jednadzbaString.split('-').join(' -')
        jednadzbaString = " "+jednadzbaString
        while(i<jednadzbaString.length)
        {
            if(jednadzbaString[i]=='x'||jednadzbaString[i]=='y')
            {
                if(jednadzbaString[i-1]==' ')
                {
                    jednadzbaString = jednadzbaString.substr(0,i-1)+"1"+jednadzbaString.substr(i,jednadzbaString.length)
                }
                if(jednadzbaString[i-1]=='-')
                {
                    jednadzbaString = jednadzbaString.substr(0,i-1)+"-1"+jednadzbaString.substr(i,jednadzbaString.length)
                }
            }
            i++
        }
        return jednadzbaString
    }

    getLeftSide()
    {
        return this.jednadzbaString.split('=')[0]
    }

    getRightSide()
    {
        return this.jednadzbaString.split('=')[1]
    }

    registerCoefficients()
    {
        var leftSide = this.getLeftSide()
        var rightSide = this.getRightSide()
        this.getCoefficients(false,leftSide)
        this.getCoefficients(true,rightSide)
    }

    getLinear()
    {
        var linearna = this.linearna.split('=')[1]
        linearna = this.adaptString(linearna)
        var i = 0
        var coef = ""
        while(i<linearna.length)
        {
            if(this.nums.includes(linearna[i]))
            {
                coef=""
                while(this.nums.includes(linearna[i]))
                {
                    coef+= linearna[i]
                    i++
                }
                if(linearna[i]=='x')
                {
                    this.a=parseFloat(coef)
                }
                else
                {
                    this.b=parseFloat(coef)
                }
            }
            i++
        }
        
    }
}