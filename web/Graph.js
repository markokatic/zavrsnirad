class Graph
{
    constructor(canvas,width,height)
    {
        this.c = canvas
        this.width = width
        this.height = height
        this.c.width = width
        this.c.height = height
        this.drawAxix()
    };

    drawAxix()
    {
        var canvasHeight = this.height
        var canvasWidth = this.width
        var context = this.c.getContext("2d")
        context.strokeStyle="#000000"
        context.beginPath()
        context.moveTo(0,(canvasHeight/2))
        context.lineTo(10,(canvasHeight/2)+10)
        context.moveTo(0,(canvasHeight/2))
        context.lineTo(10,(canvasHeight/2)-10)
        context.moveTo(0,(canvasHeight/2))
        context.lineTo(canvasWidth,(canvasHeight/2))
        context.lineTo(canvasWidth-10,(canvasHeight/2)+10)
        context.moveTo(canvasWidth,(canvasHeight/2))
        context.lineTo(canvasWidth-10,(canvasHeight/2)-10)
        context.stroke()
        
        context.beginPath()
        context.moveTo((canvasWidth/2),0)
        context.lineTo((canvasWidth/2)-10,10)
        context.moveTo((canvasWidth/2),0)
        context.lineTo((canvasWidth/2)+10,10)
        context.moveTo((canvasWidth/2),0)
        context.lineTo((canvasWidth/2),canvasHeight)
        context.lineTo((canvasWidth/2)-10,canvasHeight-10)
        context.moveTo((canvasWidth/2),canvasHeight)
        context.lineTo((canvasWidth/2)+10,canvasHeight-10)
        context.stroke()


        var i = 0
        context.beginPath()
        for(i = 0;i<canvasWidth;i = i + canvasWidth/22)
        {
            context.moveTo(i,(canvasHeight/2)-5)
            context.lineTo(i,(canvasHeight/2)+5)
        }
        context.stroke()
        context.strokeStyle = "#000000"
        context.beginPath()
        for(i = 0;i<canvasHeight;i = i + canvasHeight/22)
        {
            context.moveTo((canvasWidth/2)-5,i)
            context.lineTo((canvasWidth/2)+5,i)
        }
        context.strokeStyle = "#000000"
        context.stroke()

        context.font = "14px Arial"
        context.fillText("+X",canvasWidth-30,canvasHeight/2+25)
        context.fillText("-X",10,canvasHeight/2+25)
        context.fillText("+Y",canvasWidth/2-30,12)
        context.fillText("-Y",canvasWidth/2-30,canvasHeight-5)

    }

    getStep(top,i)
    {
        while(top>i*5)
        {
            i++
        }
        return i
    }

    drawParabola(a,b,c,stepX = 0, stepY = 0,drawTop=false,drawNullPoints=false)
    {

        var context = this.c.getContext("2d")
        var topX = -b/(2*a)
        var topY = ((4*a*c)-(b*b))/(4*a)
        if(stepX==0)
        {
            if(topX>0)
            {
                stepX = this.getStep(topX,1)
            }
            else
            {
                stepX = this.getStep(-topX,1)
            }

            context.font = "14px Arial"
            var begin = -stepX*10
            for(i = 0+this.width/22-10;i<this.width-this.width/22+20;i = i + this.width/22)
            {
                if(begin!=0){context.fillText(begin.toString(),i,this.height/2+20)}
                begin=begin+stepX
            }
        }
        if(stepY==0)
        {
            if(topY>0)
            {
                stepY = this.getStep(topY,1)
            }
            else
            {
                stepY = this.getStep(-topY,1)
            }
            context.font = "14px Arial"
            var begin = stepY*10
            for(i = 0+this.height/44;i<this.height-this.height/22;i = i + this.height/22)
            {
                if(begin!=0){context.fillText(begin.toString(),this.width/2+15,i+15)}
                begin=begin-stepY
            }
        }
        var valueXperPixel = (stepX*22)/this.width
        var valueYperPixel = (stepY*22)/this.height

        
        context.beginPath()
        var i = 0
        var y = this.height+10
        var x = -this.width/2
        var prviDio = a*(valueXperPixel*x)*(valueXperPixel*x)
        var drugiDio = b*valueXperPixel*x
        y = prviDio + drugiDio + c
        y = this.height/2 - y*(1/valueYperPixel)
        context.moveTo(i,y)
        for(i=0; i<this.width;i++)
        {
            y = 0
            x = i - this.width/2
            prviDio = a*(valueXperPixel*x)*(valueXperPixel*x)
            drugiDio = b*valueXperPixel*x
            y = prviDio + drugiDio + c

            y = this.height/2 - y*(1/valueYperPixel)
            context.lineTo(i,y)
        }
        context.strokeStyle = "#0000ff"
        context.stroke()
        context.strokeStyle="#000000"
        if(drawTop){this.drawTop(topX,topY,valueXperPixel,valueYperPixel)}
        if(drawNullPoints){this.drawNullPoints(a,b,c,valueXperPixel)}
        return [valueXperPixel,valueYperPixel]

    }

    hasRealSolutions(a,b,c)
    {
        if(((b*b)-(4*a*c))>=0)
        {
            return true
        }
        else
        {
            return false
        }
    }

    drawTop(topX,topY,valueXperPixel,valueYperPixel)
    {
        var context = this.c.getContext("2d")
        var x = topX/valueXperPixel+this.width/2
        var y = this.height/2-topY/valueYperPixel
        context.beginPath()
        context.arc(x,y,3,0,2*Math.PI)
        context.fillStyle = "#ff0000"
        context.fill()
        var string = "T("+topX+","+topY+")"
        context.fillStyle = "#000000"
        context.fillText(string,x-30,y-20)
    }

    drawNullPoints(a,b,c,valueXperPixel)
    {
        if(this.hasRealSolutions(a,b,c))
        {
            var context = this.c.getContext("2d")
            var x1 = (-b-Math.sqrt(b*b-4*a*c))/(2*a)
            var x2 = (-b+Math.sqrt(b*b-4*a*c))/(2*a)
            var x1x = x1/valueXperPixel+this.width/2
            var x2x = x2/valueXperPixel+this.width/2
            x1 = x1.toFixed(2)
            x2 = x2.toFixed(2)
            var string1 = "x1("+x1+",0)"
            var string2 = "x2("+x2+",0)"
            context.beginPath()
            context.arc(x1x,this.height/2,3,0,2*Math.PI)
            context.fillStyle = "#ff0000"
            context.fill()
            context.fillStyle = "#000000"
            context.fillText(string1,x1x+15,this.height/2-15)
            context.beginPath()
            context.arc(x2x,this.height/2,3,0,2*Math.PI)
            context.fillStyle = "#ff0000"
            context.fill()
            context.fillStyle = "#000000"
            context.fillText(string2,x2x-70,this.height/2-15)

        }
    }

    drawLinear(x)
    {
        var context = this.c.getContext("2d")
        context.font = "14px Arial"
        if(x>0)
        {
            context.beginPath()
            context.moveTo(this.width/2+this.width/22,0)
            context.lineTo(this.width/2+this.width/22,this.height)
            context.strokeStyle = "#0000ff"
            context.stroke()

            context.fillText(x.toString(),this.width/2+this.width/22,this.height/2+20)
        }
        else
        {
            context.beginPath()
            context.moveTo(this.width/2-this.width/22,0)
            context.lineTo(this.width/2-this.width/22,this.height)
            context.strokeStyle = "#0000ff"
            context.stroke()
            context.fillText(x.toString(),this.width/2-this.width/22,this.height/2+20)
        }
    }


    drawObjects(x2,y2,x,y,c,a,b)
    {
        var context = this.c.getContext("2d")
        context.beginPath()
        context.strokeStyle = "#0000ff"

        var a1 = x2+y2*a*a
        if(a1 == 0)
        {
            return "err"
        }
        var b1 = 2*y2*a*b+x+a*y
        var c1 = y2*b*b+y*b+c
        var hasIntersections=false
        var x1 = 0
        var x_2 = 0
        if((b1*b1-4*a1*c1)>=0)
        {
            hasIntersections = true
            x1 = (-b1-Math.sqrt(b1*b1-4*a1*c1))/(2*a1)
            x_2 = (-b1+Math.sqrt(b1*b1-4*a1*c1))/(2*a1)
        }
        var valueX1=x2*x1*x1+x*x1+c
        var valueX2=x2*x_2*x_2+x*x_2+c

        var y1 = 0
        var y_2 = 0

        if(y2 == 0)
        {
            y1 = x1*x1*x2/-y + x1*x/-y +c/-y
            y_2 = x_2*x_2*x2/-y + x_2*x/-y +c/-y
            var ret = this.drawParabola(x2/-y,x/-y,c/-y)
            var valueXperPixel = ret[0]
            var valueYperPixel = ret[1]
            var startPointX = -this.width/2*valueXperPixel
            var yStart = a*startPointX+b
            var yPixels =this.height/2- yStart/valueYperPixel
            var endPointX = this.width/2*valueXperPixel
            var yEnd = a*endPointX+b
            var yEndPixels = this.height/2-yEnd/valueYperPixel
            context.moveTo(0,yPixels)
            context.lineTo(this.width,yEndPixels)
            context.stroke()
            if(hasIntersections)
            {
                if(x1==x_2)
                {
                    context.beginPath()
                    context.arc(x1/valueXperPixel+this.width/2,this.height/2-y1/valueYperPixel,3,0,2*Math.PI)
                    context.fillStyle = "#ff0000"
                    var textStringX1="("+x1.toFixed(2)+","+y1.toFixed(2)+")"
                    context.fillText(textStringX1,x1/valueXperPixel+30+this.width/2,this.height/2-y1/valueYperPixel)
                    context.fill()
                    context.fillStyle = "#000000"
                    return [x1.toFixed(2),y1.toFixed(2)]
                }
                else
                {
                    context.beginPath()
                    context.arc(x1/valueXperPixel+this.width/2,this.height/2-y1/valueYperPixel,3,0,2*Math.PI)
                    context.fillStyle = "#ff0000"
                    var textStringX1="("+x1.toFixed(2)+","+y1.toFixed(2)+")"
                    context.fillText(textStringX1,x1/valueXperPixel+30+this.width/2,this.height/2-y1/valueYperPixel)
                    context.fill()
                    context.beginPath()
                    context.arc(x_2/valueXperPixel+this.width/2,this.height/2-y_2/valueYperPixel,3,0,2*Math.PI)
                    context.fillStyle = "#ff0000"
                    var textStringX2="("+x_2.toFixed(2)+","+y_2.toFixed(2)+")"
                    context.fillText(textStringX2,x_2/valueXperPixel+30+this.width/2,this.height/2-y_2/valueYperPixel)
                    context.fill()
                    context.fillStyle = "#000000"
                    return [x1.toFixed(2),y1.toFixed(2),x_2.toFixed(2),y_2.toFixed(2)]
                }
            }
            
            return null
        }

        if(hasIntersections)
        {
            y1 = (-y-Math.sqrt(y*y-4*y2*valueX1))/(2*y2)
            if(y1.toFixed(3)!=(a*x1+b).toFixed(3))
            {
                y1 = (-y+Math.sqrt(y*y-4*y2*valueX1))/(2*y2)
            }
            y_2= (-y+Math.sqrt(y*y-4*y2*valueX2))/(2*y2)
            if(y_2.toFixed(3)!=(a*x_2+b).toFixed(3))
            {
                y_2= (-y-Math.sqrt(y*y-4*y2*valueX2))/(2*y2)
            }
        }



        var stepX = 0
        var stepY = 0
        var topX = -x/2
        var topY = -y/2
        if(hasIntersections)
        {
            if(Math.abs(x1)>Math.abs(x_2))
            {
                topX=x1
            }
            else
            {
                topX=x_2
            }
            if(Math.abs(y1)>Math.abs(y_2))
            {
                topY=y1
            }
            else
            {
                topY=y_2
            }

        }
        if(stepX==0)
        {
            if(topX>0)
            {
                stepX = this.getStep(topX,1)
            }
            else
            {
                stepX = this.getStep(-topX,1)
            }

            context.font = "14px Arial"
            var begin = -stepX*10
            var i = 0
            for(i = 0+this.width/22-10;i<this.width-this.width/22+20;i = i + this.width/22)
            {
                if(begin!=0){context.fillText(begin.toString(),i,this.height/2+20)}
                begin=begin+stepX
            }
        }
        if(stepY==0)
        {
            if(topY>0)
            {
                stepY = this.getStep(topY,1)
            }
            else
            {
                stepY = this.getStep(-topY,1)
            }
            context.font = "14px Arial"
            var begin = stepY*10
            for(i = 0+this.height/44;i<this.height-this.height/22;i = i + this.height/22)
            {
                if(begin!=0){context.fillText(begin.toString(),this.width/2+15,i+15)}
                begin=begin-stepY
            }
        }
        var valueXperPixel = (stepX*22)/this.width
        var valueYperPixel = (stepY*22)/this.height
        var valueX = 0
        i = 0
        var breakWhile = false
        while(i<this.width&&breakWhile==false)
        {
            valueX = (-this.width/2)*valueXperPixel + i*valueXperPixel
            var cc = x2*valueX*valueX+x*valueX+c
            if(this.hasRealSolutions(y2,y,cc))
            {
                var yy = (-y-Math.sqrt(y*y-4*y2*cc))/(2*y2)
                var yPix = this.height/2 - yy/valueYperPixel
                context.moveTo(i,yPix)
                breakWhile = true
            }
            i+=0.01
        }
        breakWhile=false
        while(i<this.width&&breakWhile==false)
        {
            valueX = (-this.width/2)*valueXperPixel + i*valueXperPixel
            var cc = x2*valueX*valueX+x*valueX+c
            if(this.hasRealSolutions(y2,y,cc))
            {
                var yy = (-y-Math.sqrt(y*y-4*y2*cc))/(2*y2)
                var yPix = this.height/2 - yy/valueYperPixel
                context.lineTo(i,yPix)
            }
            else
            {
                breakWhile=true
            }
            i+=0.01
        }
        context.stroke()
        breakWhile=false
        while(i<this.width&&breakWhile==false)
        {
            valueX = (-this.width/2)*valueXperPixel + i*valueXperPixel
            var cc = x2*valueX*valueX+x*valueX+c
            if(this.hasRealSolutions(y2,y,cc))
            {
                var yy = (-y-Math.sqrt(y*y-4*y2*cc))/(2*y2)
                var yPix = this.height/2 - yy/valueYperPixel
                context.moveTo(i,yPix)
                breakWhile = true
            }
            i+=0.01
        }
        breakWhile=false
        while(i<this.width&&breakWhile==false)
        {
            valueX = (-this.width/2)*valueXperPixel + i*valueXperPixel
            var cc = x2*valueX*valueX+x*valueX+c
            if(this.hasRealSolutions(y2,y,cc))
            {
                var yy = (-y-Math.sqrt(y*y-4*y2*cc))/(2*y2)
                var yPix = this.height/2 - yy/valueYperPixel
                context.lineTo(i,yPix)
            }
            else
            {
                breakWhile=true
            }
            i+=0.01
        }
        breakWhile=false
        context.stroke()
        i = 0
        breakWhile=false
        while(i<this.width&&breakWhile==false)
        {
            valueX = (-this.width/2)*valueXperPixel + i*valueXperPixel
            var cc = x2*valueX*valueX+x*valueX+c
            if(this.hasRealSolutions(y2,y,cc))
            {
                var yy = (-y+Math.sqrt(y*y-4*y2*cc))/(2*y2)
                var yPix = this.height/2 - yy/valueYperPixel
                context.moveTo(i,yPix)
                breakWhile = true
            }
            i+=0.01
        }
        breakWhile=false
        while(i<this.width&&breakWhile==false)
        {
            valueX = (-this.width/2)*valueXperPixel + i*valueXperPixel
            var cc = x2*valueX*valueX+x*valueX+c
            if(this.hasRealSolutions(y2,y,cc))
            {
                var yy = (-y+Math.sqrt(y*y-4*y2*cc))/(2*y2)
                var yPix = this.height/2 - yy/valueYperPixel
                context.lineTo(i,yPix)
            }
            else
            {
                breakWhile=true
            }
            i+=0.01
        }
        context.stroke()
        breakWhile=false
        while(i<this.width&&breakWhile==false)
        {
            valueX = (-this.width/2)*valueXperPixel + i*valueXperPixel
            var cc = x2*valueX*valueX+x*valueX+c
            if(this.hasRealSolutions(y2,y,cc))
            {
                var yy = (-y+Math.sqrt(y*y-4*y2*cc))/(2*y2)
                var yPix = this.height/2 - yy/valueYperPixel
                context.moveTo(i,yPix)
                breakWhile = true
            }
            i+=0.01
        }
        breakWhile=false
        while(i<this.width&&breakWhile==false)
        {
            valueX = (-this.width/2)*valueXperPixel + i*valueXperPixel
            var cc = x2*valueX*valueX+x*valueX+c
            if(this.hasRealSolutions(y2,y,cc))
            {
                var yy = (-y+Math.sqrt(y*y-4*y2*cc))/(2*y2)
                var yPix = this.height/2 - yy/valueYperPixel
                context.lineTo(i,yPix)
            }
            else
            {
                breakWhile=true
            }
            i+=0.01
        }
        context.strokeStyle = "#0000ff"
        context.stroke()
        var startPointX = -this.width/2*valueXperPixel
        var yStart = a*startPointX+b
        var yPixels =this.height/2- yStart/valueYperPixel
        var endPointX = this.width/2*valueXperPixel
        var yEnd = a*endPointX+b
        var yEndPixels = this.height/2-yEnd/valueYperPixel
        context.moveTo(0,yPixels)
        context.lineTo(this.width,yEndPixels)
        context.stroke()
        if(hasIntersections)
        {
            if(x1==x_2)
            {
                context.beginPath()
                context.arc(x1/valueXperPixel+this.width/2,this.height/2-y1/valueYperPixel,3,0,2*Math.PI)
                context.fillStyle = "#ff0000"
                var textStringX1="("+x1.toFixed(2)+","+y1.toFixed(2)+")"
                context.fillText(textStringX1,x1/valueXperPixel+30+this.width/2,this.height/2-y1/valueYperPixel)
                context.fill()
                context.fillStyle = "#000000"
                return [x1.toFixed(2),y1.toFixed(2)]
            }
            else
            {
                context.beginPath()
                context.arc(x1/valueXperPixel+this.width/2,this.height/2-y1/valueYperPixel,3,0,2*Math.PI)
                context.fillStyle = "#ff0000"
                var textStringX1="("+x1.toFixed(2)+","+y1.toFixed(2)+")"
                context.fillText(textStringX1,x1/valueXperPixel+30+this.width/2,this.height/2-y1/valueYperPixel)
                context.fill()
                context.beginPath()
                context.arc(x_2/valueXperPixel+this.width/2,this.height/2-y_2/valueYperPixel,3,0,2*Math.PI)
                context.fillStyle = "#ff0000"
                var textStringX2="("+x_2.toFixed(2)+","+y_2.toFixed(2)+")"
                context.fillText(textStringX2,x_2/valueXperPixel+30+this.width/2,this.height/2-y_2/valueYperPixel)
                context.fill()
                context.fillStyle = "#000000"
                return [x1.toFixed(2),y1.toFixed(2),x_2.toFixed(2),y_2.toFixed(2)]
            }
        }


    }


    clearCanvas()
    {
        var context = this.c.getContext("2d")
        context.clearRect(0,0,this.width,this.height)
        this.drawAxix()
    }
}